# GovBR-DS - Config Tools

## Objetivo

Compartilhar os padrões de criação markdown entre os projetos do [GovBR-DS](https://gitlab.com/govbr-ds 'GovBR-DS').

## Como instalar

1. Instale os seguintes pacotes:

   ```bash
   npm install -D @govbr-ds/markdownlint-config husky cross-env
   ```

## Como configurar

Configurações para o [Markdownlint](https://github.com/DavidAnson/markdownlint) e também usamos o [Markdownlint-cli](https://github.com/igorshubovych/markdownlint-cli).

1. Crie um arquivo `.markdownlint.yml` na raiz do seu projeto e extenda a configuração:

   ```yml
   extends: '@govbr-ds/markdownlint-config'
   ```

1. Crie um arquivo `.markdownlintignore` na raiz do seu projeto e inclua os arquivos e pastas que deseja ignorar da validação de markdown:

   ```yml
   CHANGELOG.MD
   node_modules
   ```

   Quando o CHANGELOG.md é gerado automaticamente é recomendável o excluir da verificação.

1. Conforme a documentação do [Husky](https://github.com/typicode/husky) inclua o hook `pre-commit` com o código abaixo:

   ```bash
   npx --no -- lint-staged
   ```

   E configure o `package.json` com o seguinte script:

   ```json
     "scripts": {
        "lint:md": "cross-env markdownlint -d -f '**/*.md'",
     },
     "lint-staged": {
        "*.md": [
          "markdownlint -d -f"
        ]
     },
   ```

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](../../CONTRIBUTING.md 'Como contribuir?').

## Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/tools/govbr-ds-config-tools/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)

- Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Padrão de commits

Para mais informações sobre o padrão de commits consulte [a nossa Wiki](https://gov.br/ds/wiki/git-gitlab/guias/commit/ 'Padrão de commit').

## Licença

Nesse projeto usamos a licença MIT.
