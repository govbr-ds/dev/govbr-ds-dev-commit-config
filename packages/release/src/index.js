const branches = [
  'main',
  '+([0-9])?(.{+([0-9]),x}).x', //1.x.x
  { name: 'next', prerelease: 'next' },
  { name: 'alpha', prerelease: 'alpha' },
  { name: '+([0-9])?(.{+([0-9]),x}).x-alpha', prerelease: 'alpha' },
  // 'next-major',
  // { name: '+([0-9])?(.{+([0-9]),x}).x-beta', prerelease: 'alpha' },
  // { name: '+([0-9])?(.{+([0-9]),x}).x-rc', prerelease: 'alpha' },
  // { name: 'beta', prerelease: true },
  // { name: 'rc', prerelease: true },
]

const commitAnalyzer = [
  '@semantic-release/commit-analyzer',
  {
    preset: 'conventionalcommits',
    releaseRules: [
      { breaking: true, release: 'major' },
      { type: '*!', release: 'major' },
      { type: 'build', release: false },
      { type: 'chore', release: false },
      { type: 'ci', release: false },
      { type: 'deprecated', release: false },
      { type: 'docs', release: 'patch' },
      { type: 'feat', release: 'minor' },
      { type: 'fix', release: 'patch' },
      { type: 'ops', release: false },
      { type: 'perf', release: 'patch' },
      { type: 'refactor', release: 'patch' },
      { type: 'removed', release: 'minor' },
      { type: 'revert', release: 'minor' },
      { type: 'lint', release: false },
      { type: 'test', release: false },
      { type: 'wip', release: false },
      { scope: 'no-release', release: false },
    ],
    parserOpts: {
      noteKeywords: ['BREAKING CHANGE', 'BREAKING CHANGES', 'BREAKING'],
    },
  },
]

const releaseNotesGenerator = [
  '@semantic-release/release-notes-generator',
  {
    preset: 'conventionalcommits',
    presetConfig: {
      header: '# CHANGELOG',
      types: [
        { type: 'deprecated', section: '👎 DEPRECIADO' },
        { type: 'feat', section: '✨ NOVIDADES' },
        { type: 'removed', section: '🚧 REMOVIDO' },
        { type: 'docs', section: '📚 DOCUMENTAÇÃO' },
        { type: 'fix', section: '🐛 CORREÇÕES' },
        { type: 'ops', section: '🔧 ATIVIDADES OPERACIONAIS' },
        { type: 'perf', section: '🚀 PERFORMANCE' },
        { type: 'refactor', section: '🔁 REFATORADO' },
        { type: 'revert', section: '⏪ REVERTIDO' },
        { type: 'build', hidden: true },
        { type: 'chore', hidden: true },
        { type: 'ci', hidden: true },
        { type: 'lint', hidden: true },
        { type: 'test', hidden: true },
        { type: 'wip', hidden: true },
      ],
    },
    parserOpts: {
      noteKeywords: ['BREAKING CHANGE', 'BREAKING CHANGES', 'BREAKING'],
    },
    writerOpts: {
      groupBy: 'type',
      commitsSort: ['scope', 'subject', 'header'],
      finalizeContext: (context) => {
        const date = new Date()
        context.date = date.toLocaleString('pt-BR', {
          timeZone: 'America/Sao_Paulo',
          dateStyle: 'short',
        })
        return context
      },
      mainTemplate: `{{> header}}
{{#if noteGroups}}
{{#each noteGroups}}

### 💥 {{title}}

{{#each notes}}
* {{#if commit.scope}}**{{commit.scope}}:** {{/if}}{{text}}
{{/each}}
{{/each}}
{{/if}}
{{#each commitGroups}}

{{#if title}}
### {{title}}

{{/if}}
{{#each commits}}
{{> commit root=@root}}
{{/each}}
{{/each}}
`,
    },
  },
]

const changelog = [
  '@semantic-release/changelog',
  {
    changelogTitle: '# CHANGELOG',
  },
]

const gitlab = [
  '@semantic-release/gitlab',
  {
    successComment:
      ':tada: Issue/Merge Request incluído(a) na versão ${nextRelease.version} :tada:\n\nPara mais detalhes:\n- [GitLab release](${CI_PROJECT_URL}/-/releases/${nextRelease.version})\n- [GitLab tag](${CI_PROJECT_URL}/-/tags/${nextRelease.version})',
    failComment:
      'A release a partir da branch ${branch.name} falhou devido aos seguintes erros:\n- ${errors.map(err => err.message).join("\\n- ")}',
    failTitle: 'A release automática falhou 🚨',
    labels: 'crítico, precisa-de-triagem, semantic-release',
  },
]

const git = [
  '@semantic-release/git',
  {
    assets: ['package.json'],
    message:
      'chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes} \n\nCommit gerado automaticamente durante o processo de release',
  },
]

export { branches, changelog, commitAnalyzer, git, gitlab, releaseNotesGenerator }
