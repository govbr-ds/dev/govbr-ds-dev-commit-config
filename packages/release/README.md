# GovBR-DS - Config Tools

## Objetivo

Compartilhar os padrões de geração de release entre os projetos do [GovBR-DS](https://gitlab.com/govbr-ds 'GovBR-DS').

## Como instalar

Configurações para o [Semantic Release](https://github.com/semantic-release/semantic-release).

1. Instale os seguintes pacotes:

   ```bash
   npm install -D @govbr-ds/release-config
   ```

## Como configurar

1. Crie um arquivo `release.config.js` na raiz do seu projeto e importe os plugins que deseja usar:

   ```javascript
   import { branches, commitAnalyzer, releaseNotesGenerator, changelog, gitlab, git } from './node_modules/@govbr-ds/config-tools/src/semantic-release.cjs'

    export defaul {
     branches: branches,
     plugins: [
       commitAnalyzer,
       releaseNotesGenerator,
       changelog,
       gitlab,
       git,
       ...
     ],
   }
   ```

   Para sobrescrever alguma configuração siga o padrão do [Semantic Release](https://github.com/semantic-release/semantic-release). A ordem definida será a ordem de execução!

## Method Date.prototype.toString called on incompatible receiver

A versão 14 do `@semantic-release/release-notes-generator` introduziu um bug com o `conventional-changelog-conventionalcommits`:

```log
TypeError: Method Date.prototype.toString called on incompatible receiver [object Date]
    at Proxy.toString (<anonymous>)
    at [Symbol.toPrimitive] (<anonymous>)
    at new Date (<anonymous>)
    at Object.formatDate (file:///home/runner/work/clean-self-hosted-runner/clean-self-hosted-runner/node_modules/conventional-changelog-writer/dist/utils.js:8:12)
    at defaultCommitTransform (file:///home/runner/work/clean-self-hosted-runner/clean-self-hosted-runner/node_modules/conventional-changelog-writer/dist/options.js:23:23)
    at transformCommit (file:///home/runner/work/clean-self-hosted-runner/clean-self-hosted-runner/node_modules/conventional-changelog-writer/dist/commit.js:29:23)
    at write (file:///home/runner/work/clean-self-hosted-runner/clean-self-hosted-runner/node_modules/conventional-changelog-writer/dist/writers.js:39:28)
    at async nextAsync (node:internal/streams/from:182:33) {
  pluginName: '@semantic-release/release-notes-generator'
```

Os detalhes da causa não são importantes agora, mas caso esteja com o problema tente atualizar suas dependências para uma nova versão que pode já ter corrigido o bug.

Outras alternativas para corrigir o bug são:

1. Incluir a dependência `"conventional-changelog-conventionalcommits": "^8.0.0"` no seu `package.json`.
1. Fazer o downgrade das suas dependências do semantic-release

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](../../CONTRIBUTING.md 'Como contribuir?').

## Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/tools/govbr-ds-config-tools/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)

- Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Padrão de commits

Para mais informações sobre o padrão de commits consulte [a nossa Wiki](https://gov.br/ds/wiki/git-gitlab/guias/commit/ 'Padrão de commit').

## Licença

Nesse projeto usamos a licença MIT.
