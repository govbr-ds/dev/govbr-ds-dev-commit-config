export default {
  defaultIgnores: true,
  extends: ['@commitlint/config-conventional'],
  formatter: '@commitlint/format',
  helpUrl: 'https://gov.br/ds/wiki/git-gitlab/guias/commit/',
  ignores: [(commit) => commit.includes('[skip ci]')],
  prompt: {
    messages: {
      emptyWarning: 'Item obrigatório. Por favor preencha conforme orientação.',
      lowerLimitWarning: 'Abaixo do limite de caracteres',
      max: 'Máximo de %d caracteres',
      min: 'Mínimo de %d caracteres',
      skip: '(OPCIONAL)',
      upperLimitWarning: 'Acima do limite de caracteres',
    },
    questions: {
      body: {
        description: 'Descrição DETALHADA da mudança (Use \\n para quebrar linhas)',
      },
      breaking: {
        description: 'Descrição BREVE da(s) BREAKING CHANGE(S)',
      },
      breakingBody: {
        description:
          'A descrição DETALHADA é obrigatória para BREAKING CHANGE(S). Descreva EM DETALHES a mudança. (Use \\n para quebrar linhas)',
      },
      isBreaking: {
        description: 'Existe BREAKING CHANGE?',
      },
      isIssueAffected: {
        description: 'Essa mudança relaciona/fecha alguma(s) issue(s)?',
      },
      issues: {
        description:
          'Liste a(s) issue(s) (Veja https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)',
      },
      issuesBody: {
        description:
          'A descrição DETALHADA é obrigatória quando um commit relaciona/fecha uma issue. Descreva EM DETALHES a mudança. (Use \\n para quebrar linhas)',
      },
      scope: {
        description: 'Qual é o escopo dessa mudança? (ex: button, table, package.json)',
      },
      subject: {
        description: 'Descrição BREVE sobre a mudança (Veja https://gov.br/ds/wiki/git-gitlab/guias/commit/)',
      },
      type: {
        description:
          'ANTES DE ENVIAR UM COMMIT, LEIA A NOSSA DOCUMENTAÇÃO SOBRE O ASSUNTO: https://gov.br/ds/wiki/git-gitlab/guias/commit/\n\n Selecione o tipo da mudança',
        enum: {
          build: {
            description: 'Mudanças no sistema de build (ex: npm, node, webpack, vite...)',
          },
          chore: {
            description: 'Alterações diversas que não se encaixam em outros tipos',
          },
          ci: {
            description:
              'Alterações nos arquivos e scripts de configuração de ambiente (Gitlab, Pipelines, Permissões...)',
          },
          deprecated: {
            description: 'Marca o recurso como obsoleto. Provavelmente será removido em uma próxima versão',
          },
          docs: {
            description: 'Atividades de documentação (tutorial, guia, etc...)',
          },
          feat: {
            description: 'Nova feature, recurso, elemento, comportamento, etc...',
          },
          fix: {
            description: 'Correção em feature, recurso, elemento, comportamento, etc...',
          },
          lint: {
            description:
              'Mudanças que não alteram o significado/comportamento (espaços, formatação, semi-vírgulas ausentes...)',
          },
          ops: {
            description: 'Atividades operacionais relacionadas a design e desenvolvimento',
          },
          perf: {
            description: 'Melhoria de desempenho (tempo de execução, tamanho, carregamento...)',
          },
          refactor: {
            description: 'Altera o conteúdo sem mudar o resultado final (ex: organização de pastas, camadas...)',
          },
          removed: {
            description: 'Recurso excluído definitivamente do projeto',
          },
          revert: {
            description: 'Reverte um commit',
          },
          test: {
            description: 'Atividades relacionadas a testes',
          },
          wip: {
            description: 'Trabalho ainda não finalizado',
          },
        },
      },
    },
  },
  // https://commitlint.js.org/#/reference-rules
  rules: {
    'body-full-stop': [0, 'always', '.'],
    'body-leading-blank': [2, 'always'],
    'body-max-length': [0, 'always', Infinity],
    'body-max-line-length': [0, 'always', Infinity],
    'footer-leading-blank': [2, 'always'],
    'footer-max-length': [0, 'always', Infinity],
    'footer-max-line-length': [0, 'always', Infinity],
    'header-case': [0, 'always', 'lower-case'],
    'header-full-stop': [0, 'always', '.'],
    'header-max-length': [0, 'always', Infinity],
    'header-min-length': [2, 'always', 20],
    'scope-case': [0, 'always', 'lower-case'],
    'scope-max-length': [0, 'always', Infinity],
    'subject-case': [0, 'always', 'lower-case'],
    'subject-full-stop': [0, 'always', '.'],
    'subject-max-length': [0, 'always', Infinity],
    'type-case': [0, 'always', 'lower-case'],
    'type-enum': [
      2,
      'always',
      [
        'build',
        'chore',
        'ci',
        'deprecated',
        'docs',
        'feat',
        'fix',
        'lint',
        'ops',
        'perf',
        'refactor',
        'removed',
        'revert',
        'test',
        'wip',
      ],
    ],
    'type-max-length': [2, 'always', Infinity],
  },
}
