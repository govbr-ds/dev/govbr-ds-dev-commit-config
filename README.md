# GovBR-DS - Config Tools

Monorepo para configurações de pacotes comumente utilizados nos projetos GovBR-DS.

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](CONTRIBUTING.md 'Como contribuir?').

## Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/tools/govbr-ds-config-tools/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)

- Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Padrão de commits

Para mais informações sobre o padrão de commits consulte [a nossa Wiki](https://gov.br/ds/wiki/git-gitlab/guias/commit/ 'Padrão de commit').

## Licença

Nesse projeto usamos a licença MIT.
