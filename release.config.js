import { branches, commitAnalyzer, gitlab, releaseNotesGenerator } from './packages/release/src/index.js'

export default {
  branches: branches,
  plugins: [
    commitAnalyzer,
    releaseNotesGenerator,
    [
      '@semantic-release/exec',
      {
        prepareCmd: 'npx nx release version ${nextRelease.version}',
        publishCmd: 'npx nx release publish --tag ${nextRelease.channel}',
      },
    ],
    [
      '@semantic-release/git',
      {
        assets: [
          'packages/commitlint/package.json',
          'packages/markdownlint/package.json',
          'packages/release/package.json',
        ],
        message:
          'chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes} \n\nCommit gerado automaticamente durante o processo de release',
      },
    ],
    gitlab,
  ],
}
