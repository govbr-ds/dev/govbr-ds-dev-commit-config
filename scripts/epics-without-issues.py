# Script para encontrar épicos sem issues abertas dentro de um grupo do gitlab.com
# Retorna os links para os épicos

import requests

API_TOKEN = ''
BASE_URL = 'https://gitlab.com/api/v4'
GROUP_ID = '' # ID grupo govbr-ds:

GROUP_RESPONSE = requests.get(
    f'{BASE_URL}/groups/{GROUP_ID}',
    headers={'PRIVATE-TOKEN': API_TOKEN}
)

if GROUP_RESPONSE.status_code == 200:
    GROUP_DATA = GROUP_RESPONSE.json()
    GROUP_NAME = GROUP_DATA['path']
    print(f"Nome do grupo: {GROUP_NAME}")
else:
    print(f"Erro ao obter o grupo: {GROUP_RESPONSE.status_code}")
    exit(1)

EPICS_RESPONSE = requests.get(
    f'{BASE_URL}/groups/{GROUP_ID}/epics',
    headers={'PRIVATE-TOKEN': API_TOKEN}
)

if EPICS_RESPONSE.status_code == 200:
    EPICS = EPICS_RESPONSE.json()
else:
    print(f"Erro ao obter épicos: {EPICS_RESPONSE.status_code}")
    exit(1)

EPICS_WITHOUT_OPEN_ISSUES = []
for EPIC in EPICS:
    EPIC_ID = EPIC['id']
    EPIC_IID = EPIC['iid']

    ISSUES_RESPONSE = requests.get(
        f'{BASE_URL}/groups/{GROUP_ID}/issues?epic_id={EPIC_ID}',
        headers={'PRIVATE-TOKEN': API_TOKEN}
    )

    if ISSUES_RESPONSE.status_code == 200:
        ISSUES = ISSUES_RESPONSE.json()

        OPEN_ISSUES = [issue for issue in ISSUES if issue['state'] == 'opened']

        if not OPEN_ISSUES:

            EPIC_URL = f"https://gitlab.com/groups/{GROUP_NAME}/-/epics/{EPIC_IID}"
            EPICS_WITHOUT_OPEN_ISSUES.append(EPIC_URL)
    else:
        print(f"Erro ao obter issues para o épico {EPIC_ID}: {issues_response.status_code}")

for EPIC_URL in EPICS_WITHOUT_OPEN_ISSUES:
    print(EPIC_URL)
