#!/bin/bash

# Script para criar issues em lote dentro do gitlab.com

# Função para criar uma issue no GitLab
create_gitlab_issue() {
  local project_id="$1"
  local title="$2"
  local description="$3"
  local gitlab_token="$4"
  local labels="$5"

  # URL da API do GitLab para criar uma issue
  local url="https://gitlab.com/api/v4/projects/${project_id}/issues"

  # Requisição POST para criar a issue
  local response=$(curl -s --request POST \
    --header "PRIVATE-TOKEN: ${gitlab_token}" \
    --form "title=${title}" \
    --form "description=${description}" \
    --form "labels=${labels}" \
    "${url}")

  # Verifica se a requisição foi bem-sucedida
  if [[ $(echo "${response}" | grep -c '"id"') -gt 0 ]]; then
    echo "Issue \"${title}\" criada com sucesso!"
  else
    echo "Falha ao criar a issue \"${title}\":"
    echo "${response}"
  fi
}

# Função principal
main() {
  local project_id=""                                   # O ID do projeto no GitLab
  local gitlab_token=""                                 # Gere um token de acesso pessoal do GitLab com permissões de 'api'
  local labels="categoria::componente, dev, tipo::novo" # Labels que deseja atribuir à issue
  local names=("Avatar" "Breadcrumb")                   # Lista de nomes para os títulos das issues

  local content=$(
    cat << EOF
## Titulo

Conteúdo
EOF
  )

  # Para cada nome, criar uma nova issue
  for name in "${names[@]}"; do
    title="Criar ${name}"
    create_gitlab_issue "${project_id}" "${title}" "${content}" "${gitlab_token}" "${labels}"
  done
}

# Executa a função principal
main
